package ppss;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.Month;

import static org.junit.jupiter.api.Assertions.*;

class AlquilaCochesTest {
    AlquilaCoches a = new AlquilaCoches();
    ServicioStub s = new ServicioStub();

    @Test
    public void casoDePrueba1(){
        LocalDate fecha = LocalDate.of(2021, Month.MAY, 18);

        a.calendario = new CalendarioStub();
        Ticket resultadoEsperado = new Ticket();
        resultadoEsperado.setPrecio_final(75.0f);
        assertDoesNotThrow(() -> a.calculaPrecio(TipoCoche.TURISMO, fecha, 10, s), "Excepcion lanzada");
        assertAll(
                () -> assertEquals(resultadoEsperado, a.calculaPrecio(TipoCoche.TURISMO, fecha, 10, s))
        );
    }

    @Test
    public void casoDePrueba2(){
        LocalDate fecha = LocalDate.of(2021, Month.JUNE, 19);
        a.calendario = new CalendarioStub();
        Ticket resultadoEsperado = new Ticket();
        resultadoEsperado.setPrecio_final(62.5f);
        assertDoesNotThrow(() -> a.calculaPrecio(TipoCoche.CARAVANA, fecha, 7, s), "Excepcion lanzada");
        assertAll(
                () -> assertEquals(resultadoEsperado, a.calculaPrecio(TipoCoche.CARAVANA, fecha, 7, s))
        );
    }

    @Test
    public void casoDePrueba3(){
        LocalDate fecha = LocalDate.of(2021, Month.APRIL, 17);
        a.calendario = new CalendarioStub();
        MensajeException resultadoReal = assertThrows(MensajeException.class,
                () -> a.calculaPrecio(TipoCoche.TURISMO, fecha, 8, s));

        assertEquals("Error en dia: 2021-04-18; " + "Error en dia: 2021-04-21; " + "Error en dia: 2021-04-22; ",
	        resultadoReal.getMessage());
    }
}
