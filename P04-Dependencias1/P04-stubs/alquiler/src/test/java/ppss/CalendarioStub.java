package ppss;

import java.time.LocalDate;
import java.time.Month;

public class CalendarioStub extends Calendario{

    @Override
    public boolean es_festivo(LocalDate dia) throws CalendarioException {
        if(dia.getMonth() == Month.MAY){
            return false;
        }
        else if(dia.getMonth() == Month.JUNE){
            if(dia.getDayOfMonth() == 20 || dia.getDayOfMonth() == 24){
                return true;
            }
            else{
                return false;
            }
        }
        else{
            if(dia.getDayOfMonth() == 18 || dia.getDayOfMonth() == 21 || dia.getDayOfMonth() == 22){
                throw new CalendarioException("Error en dia: " + dia.getDayOfMonth() + "; ");
            }
            else{
                return false;
            }
        }
    }
}
