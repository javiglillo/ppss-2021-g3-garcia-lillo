package ppss.ejercicio1;

import ppss.ejercicio1.GestorLlamadas;
import ppss.ejercicio2.CalendarioStub;

public class GestorLlamadasStub extends GestorLlamadas {
    int result;

    public void setResult(int result){
        this.result = result;
    }

    @Override
    public int getHoraActual(){
        return result;
    }


}
