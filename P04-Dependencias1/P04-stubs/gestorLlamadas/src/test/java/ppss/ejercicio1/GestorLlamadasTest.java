package ppss.ejercicio1;

import org.junit.jupiter.api.Test;
import ppss.ejercicio1.GestorLlamadasStub;

import static org.junit.jupiter.api.Assertions.*;

class GestorLlamadasTest {
    GestorLlamadasStub gestor = new GestorLlamadasStub();

    @Test
    public void casoDePrueba1(){
        gestor.setResult(15);
        double resultadoReal = gestor.calculaConsumo(10);
        assertEquals(208.0, resultadoReal);
    }

    @Test
    public void casoDePrueba2(){
        gestor.setResult(22);
        double resultadoReal = gestor.calculaConsumo(10);
        assertEquals(105.0, resultadoReal);
    }
}