package ppss.ejercicio2;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class GestorLlamadasTest {
    GestorLlamadasStub gestor = new GestorLlamadasStub();
    CalendarioStub calendario = new CalendarioStub();

    @Test
    public void casoDePrueba1(){
        calendario.setResult(15);
        gestor.setCalendario(calendario);
        double resultadoReal = gestor.calculaConsumo(10);
        assertEquals(208.0, resultadoReal);
    }

    @Test
    public void casoDePrueba2(){
        calendario.setResult(22);
        gestor.setCalendario(calendario);
        double resultadoReal = gestor.calculaConsumo(10);
        assertEquals(105.0, resultadoReal);
    }
}