package ppss.ejercicio2;

public class GestorLlamadasStub extends GestorLlamadas{
    CalendarioStub calendario;

    public void setCalendario(CalendarioStub c){
        calendario = c;
    }

    @Override
    public Calendario getCalendario(){
        return calendario;
    }
}
