package ppss.ejercicio2;

public class CalendarioStub extends Calendario{
    int result;

    public void setResult(int result){
        this.result = result;
    }

    @Override
    public int getHoraActual(){
        return result;
    }
}
