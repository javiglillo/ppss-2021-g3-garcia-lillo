package ppss;

import ppss.excepciones.*;

import java.sql.JDBCType;
import java.util.ArrayList;

public class Reserva {
    public boolean compruebaPermisos(String login, String password, Usuario tipoUsu){
        throw new UnsupportedOperationException("Not yet implemented");
    }

    public IOperacionBO getOperacion(){
        IOperacionBO o = new Operacion();
        return o;
    }

    public void realizaReserva(String login, String password, String socio, String [] isbns) throws Exception{
        ArrayList<String> errores = new ArrayList<>();

        if(!compruebaPermisos(login, password, Usuario.BIBLIOTECARIO)){ //dependencia1
            errores.add("ERROR de permisos");
        }
        else{
            IOperacionBO io = getOperacion();
            try{
                for(String isbn : isbns){
                    try{
                        io.operacionReserva(socio, isbn); //dependencia2
                    } catch(IsbnInvalidoException iie){
                        errores.add("ISBN invalido" + ":" + isbn);
                    }
                }
            } catch(SocioInvalidoException sie){
                errores.add("SOCIO invalido");
            } catch(JDBCException je){
                errores.add("CONEXION invalida");
            }
        }
        if(errores.size() > 0){
            String mensajeError = "";
            for(String error : errores){
                mensajeError += error + "; ";
            }
            throw new ReservaException(mensajeError);
        }
    }
}
