package ppss;

public class ReservaStub extends Reserva{
    protected OperacionStub o = new OperacionStub();
    
    public void setOperacion(OperacionStub o){
        this.o = o;
    }

    @Override
    public IOperacionBO getOperacion(){
        return o;
    }

    @Override
    public boolean compruebaPermisos(String login, String password, Usuario tipoUsu){
        if(login == "ppss" && password == "ppss" && tipoUsu == Usuario.BIBLIOTECARIO){
            return true;
        }
        else{
            return false;
        }
    }
}
