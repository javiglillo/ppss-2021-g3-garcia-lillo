package ppss;

import org.junit.jupiter.api.Test;
import ppss.excepciones.ReservaException;


import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class ReservaTest {
    ReservaStub reserva = new ReservaStub();

    @Test
    public void casoDePrueba1(){
        String[] isbns = {"11111"};
        ReservaException resultadoEsperado = assertThrows(ReservaException.class,
                () -> reserva.realizaReserva("xxxx", "xxxx", "Luis", isbns));

        assertEquals(resultadoEsperado.getMessage(), "ERROR de permisos; ");
    }

    @Test
    public void casoDePrueba2(){
        String[] isbns = {"11111", "22222"};
        assertDoesNotThrow(() -> reserva.realizaReserva("ppss", "ppss", "Luis", isbns), "Excepcion lanzada");
    }

    @Test
    public void casoDePrueba3(){
        String[] isbns = {"33333"};
        ReservaException resultadoEsperado = assertThrows(ReservaException.class,
                () -> reserva.realizaReserva("ppss", "ppss", "Luis", isbns));

        assertEquals(resultadoEsperado.getMessage(), "ISBN invalido:33333; ");
    }

    @Test
    public void casoDePrueba4(){
        String[] isbns = {"11111"};
        ReservaException resultadoEsperado = assertThrows(ReservaException.class,
                () -> reserva.realizaReserva("ppss", "ppss", "Pepe", isbns));

        assertEquals(resultadoEsperado.getMessage(), "SOCIO invalido; ");
    }

    @Test
    public void casoDePrueba5(){
        String[] isbns = {"11111"};
        OperacionStub o = new OperacionStub();
        o.setLanzaExcepcionBD(true);
        reserva.setOperacion(o);

        ReservaException resultadoEsperado = assertThrows(ReservaException.class,
                () -> reserva.realizaReserva("ppss", "ppss", "Luis", isbns));

        assertEquals(resultadoEsperado.getMessage(), "CONEXION invalida; ");
    }
}