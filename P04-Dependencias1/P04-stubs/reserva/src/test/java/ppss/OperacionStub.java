package ppss;

import ppss.excepciones.IsbnInvalidoException;
import ppss.excepciones.JDBCException;
import ppss.excepciones.SocioInvalidoException;

public class OperacionStub implements IOperacionBO{
    boolean lanzaExcepcionBD = false;

    public void setLanzaExcepcionBD(boolean l){
        lanzaExcepcionBD = l;
    }
    @Override
    public void operacionReserva(String socio, String isbn) throws IsbnInvalidoException, JDBCException, SocioInvalidoException {
        if(socio != "Luis"){
            throw new SocioInvalidoException();
        }
        if(isbn != "11111" && isbn != "22222"){
            throw new IsbnInvalidoException();
        }
        if(lanzaExcepcionBD){
            throw new JDBCException();
        }
    };
}
