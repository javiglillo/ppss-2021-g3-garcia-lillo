package ejercicio2.conPO;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ManagerPage {
    WebDriver driver;
    WebElement mensajeBienvenida;

    public ManagerPage(WebDriver driver){
        this.driver = driver;
    }
    public String getPageMessage(){
        mensajeBienvenida = driver.findElement(By.className("heading3"));
        return mensajeBienvenida.getText();
    }
}
