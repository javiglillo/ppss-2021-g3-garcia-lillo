package ejercicio2.conPO;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginPage {
    WebDriver driver;
    WebElement userID;
    WebElement password;
    WebElement login;
    WebElement ptitle;

    public LoginPage(WebDriver driver){
        this.driver = driver;
        this.driver.get("http://demo.guru99.com/V5");
        userID = this.driver.findElement(By.name("uid"));
        password = driver.findElement(By.name("password"));
        login = driver.findElement(By.name("btnLogin"));
    }

    public void login(String user, String pass){
        userID.sendKeys(user);
        password.sendKeys(pass);
        login.click();
    }

    public String getMessageAlert(){
        Alert alert = driver.switchTo().alert();
        String mensaje = alert.getText();
        alert.accept();

        return mensaje;
    }
}
