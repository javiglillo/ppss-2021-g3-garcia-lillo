package ejercicio3.conPOyPFact;

import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.time.LocalDate;

public class NewCustomerPage {
    WebDriver driver;
    @FindBy(className = "heading3") WebElement mensajeInicio;
    @FindBy(name = "name") WebElement customerName;
    @FindBy(css = "input[value='m']") WebElement genderMale;
    @FindBy(css = "input[value='f']") WebElement genderFemale;
    @FindBy(name = "dob") WebElement date;
    @FindBy(name = "addr") WebElement address;
    @FindBy(name = "city") WebElement city;
    @FindBy(name = "state") WebElement state;
    @FindBy(name = "pinno") WebElement pin;
    @FindBy(name = "telephoneno") WebElement telephone;
    @FindBy(name = "emailid") WebElement email;
    @FindBy(name = "password") WebElement password;
    @FindBy(name = "sub") WebElement btnsubmit;


    public NewCustomerPage(WebDriver driver){
        this.driver = driver;
    }

    public String getPageMessage(){
        return mensajeInicio.getText();
    }

    public void completeForm(String name, String gender, String day, String month, String year, String address, String city, String state, String pin, String telephone, String email, String password){
        customerName.sendKeys(name);
        if(gender == "m"){
            genderMale.click();
        }
        else if(gender == "f"){
            genderFemale.click();
        }
        this.date.sendKeys(day, month, year);
        this.address.sendKeys(address);
        this.state.sendKeys(state);
        this.city.sendKeys(city);
        this.pin.sendKeys(pin);
        this.telephone.sendKeys(telephone);
        this.email.sendKeys(email);
        this.password.sendKeys(password);
        try {
            ScreenShoot.captura(driver, "Cliente");
        }catch(IOException e) { }
        JavascriptExecutor js = (JavascriptExecutor) driver;

        js.executeScript("arguments[0].scrollIntoView();", btnsubmit);
        btnsubmit.click();
    }

    public void setDriver(WebDriver driver){
        this.driver = driver;
    }

    public String getMessageAlert(){
        Alert alert = driver.switchTo().alert();
        String mensaje = alert.getText();
        alert.accept();

        return mensaje;
    }
}
