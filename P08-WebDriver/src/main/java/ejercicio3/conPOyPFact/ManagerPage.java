package ejercicio3.conPOyPFact;


import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;

public class ManagerPage {
    WebDriver driver;
    @FindBy(className = "heading3") WebElement mensajeBienvenida;
    @FindBy(linkText = "New Customer") WebElement newCustomer;
    @FindBy(xpath = "//table/tbody/tr/td/table/tbody/tr[4]/td[2]") WebElement userID;

    public ManagerPage(WebDriver driver){
        this.driver = driver;
    }
    public String getPageMessage(){
        return mensajeBienvenida.getText();
    }

    public String getId(){
        return userID.getText();
    }

    public void newCustomerClick(){
        newCustomer.click();
    }
}
