package ejercicio3.conPOyPFact;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class DeleteCustomerPage {
    WebDriver driver;
    @FindBy(className = "heading3") WebElement mensajeInicio;
    @FindBy(name = "cusid") WebElement idCustomer;
    @FindBy(name = "AccSubmit") WebElement btnsubmit;

    public DeleteCustomerPage(WebDriver driver){
        this.driver = driver;
    }

    public String getPageMessage(){
        return mensajeInicio.getText();
    }

    public void deleteCustomer(String id){
        idCustomer.sendKeys(id);
        btnsubmit.click();
        Alert alert = driver.switchTo().alert();
        alert.accept();
        alert = driver.switchTo().alert();
        alert.accept();
    }
}
