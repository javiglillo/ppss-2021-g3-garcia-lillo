package ejercicio1.sinPageObject;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class TestLogin {
    WebDriver driver;

    @BeforeEach
    public void comunLinesTest(){
        driver = new ChromeDriver();

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("http://demo.guru99.com/V5");
    }
    @Test
    public void test_Login_Correct(){
        driver.findElement(By.name("uid")).sendKeys("mngr324480");
        driver.findElement(By.name("password")).sendKeys("jymEryh");

        WebElement botonLogin = driver.findElement(By.name("btnLogin"));
        botonLogin.click();
        WebElement mensajeBienvenida = driver.findElement(By.className("heading3"));
        Assertions.assertEquals("Welcome To Manager's Page of Guru99 Bank", mensajeBienvenida.getText());
    }

    @Test
    public void test_login_Incorrect(){
        driver.findElement(By.name("uid")).sendKeys("malid");
        driver.findElement(By.name("password")).sendKeys("malpassword");

        WebElement botonLogin = driver.findElement(By.name("btnLogin"));
        botonLogin.click();

        Alert alert = driver.switchTo().alert();
        String mensaje = alert.getText();
        Assertions.assertEquals("User is not valid", mensaje);
        alert.accept();
    }

    @AfterEach
    public void closeDriver(){
        driver.close();
    }
}
