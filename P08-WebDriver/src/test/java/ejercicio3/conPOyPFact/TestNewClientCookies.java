package ejercicio3.conPOyPFact;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.PageFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

public class TestNewClientCookies {
    WebDriver driver;
    String customerID; //id del cliente registrado
   
    @BeforeEach
    public void antes(){
        ChromeOptions chromeOptions = new ChromeOptions();
        boolean headless = Boolean.parseBoolean(System.getProperty("chromeHeadless"));
        chromeOptions.setHeadless(headless);
        driver = new ChromeDriver(chromeOptions);
        Cookies.storeCookiesToFile("mngr324480", "jymEryh");
        Cookies.loadCookiesFromFile(driver);
    }

    @Test
    public void testTestNewClientOk(){
        //DATOS DE ENTRADA
        String name = "jgl", gender = "m", address = "Calle x", city = "Alicante", state = "Alicante",
                pin = "123456", telephone = "999999999", email = "jgl823@alu.ua.es", password = "123456";
        String day = "26", month = "02", year = "2000";
        //////////////////////////
        driver.get("http://demo.guru99.com/V5/manager/Managerhomepage.php");
        ManagerPage manager = PageFactory.initElements(driver, ManagerPage.class);
        Assertions.assertEquals("Welcome To Manager's Page of Guru99 Bank", manager.getPageMessage()); //AQUI ESTAMOS EN LA PAG DE BIENVENIDA

        manager.newCustomerClick(); //vamos al formulario de adición del cliente

        driver.get("http://demo.guru99.com/V5/manager/addcustomerpage.php");
        NewCustomerPage customerPage = PageFactory.initElements(driver, NewCustomerPage.class);
        Assertions.assertEquals("Add New Customer Form", customerPage.getPageMessage());

        //REGISTRAMOS AL NUEVO CLIENTE
        customerPage.completeForm(name, gender, day, month, year, address, city, state, pin, telephone, email, password);
        customerID = manager.getId();
        //COMPROBAMOS QUE SE HA CREADO CORRECTAMENTE Y CAPTURAMOS EL ID
        Assertions.assertEquals("Customer Registered Successfully!!!", manager.getPageMessage());
    }

    @Test
    public void testTestNewClientDuplicate(){
        //DATOS DE ENTRADA
        String name = "jgl", gender = "m", address = "Calle x", city = "Alicante", state = "Alicante",
                pin = "123456", telephone = "999999999", email = "jgl823@alu.ua.es", password = "123456";
        String day = "26", month = "02", year = "2000";
        //////////////////////////
        driver.get("http://demo.guru99.com/V5/manager/Managerhomepage.php");
        ManagerPage manager = PageFactory.initElements(driver, ManagerPage.class);
        Assertions.assertEquals("Welcome To Manager's Page of Guru99 Bank", manager.getPageMessage()); //AQUI ESTAMOS EN LA PAG DE BIENVENIDA

        manager.newCustomerClick(); //vamos al formulario de adición del cliente

        driver.get("http://demo.guru99.com/V5/manager/addcustomerpage.php");
        NewCustomerPage customerPage = PageFactory.initElements(driver, NewCustomerPage.class);
        Assertions.assertEquals("Add New Customer Form", customerPage.getPageMessage());

        //REGISTRAMOS AL NUEVO CLIENTE
        customerPage.completeForm(name, gender, day, month, year, address, city, state, pin, telephone, email, password);
        Assertions.assertEquals("Customer Registered Successfully!!!", customerPage.getPageMessage());
        customerID = manager.getId();
        manager.newCustomerClick();

        Assertions.assertEquals("Add New Customer Form", customerPage.getPageMessage());
        //////////////////////////
        //REGISTRAMOS AL NUEVO CLIENTE DUPLICANDO EL EMAIL DEL ANTERIOR
        customerPage.completeForm(name, gender, day, month, year, address, city, state, pin, telephone, email, password);

        //COMPROBAMOS EL MENSAJE DE ALERTA POR LA DUPLICACION DEL EMAIL
        Assertions.assertEquals("Email Address Already Exist !!", customerPage.getMessageAlert());

    }

    @AfterEach
    public void deleteClient(){
        driver.get("http://demo.guru99.com/V5/manager/DeleteCustomerInput.php");
        DeleteCustomerPage deleteCustomerPage = PageFactory.initElements(driver, DeleteCustomerPage.class);

        deleteCustomerPage.deleteCustomer(customerID);
        driver.close();
    }
}
