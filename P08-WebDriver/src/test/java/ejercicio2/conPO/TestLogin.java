package ejercicio2.conPO;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class TestLogin {
    WebDriver driver;

    @BeforeEach
    public void comunLinesTest(){
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @Test
    public void test_Login_Correct(){
        LoginPage login = new LoginPage(driver);
        login.login("mngr324480", "jymEryh");
        driver.get("http://demo.guru99.com/V5/manager/Managerhomepage.php");
        ManagerPage manager = new ManagerPage(driver);

        Assertions.assertEquals("Welcome To Manager's Page of Guru99 Bank", manager.getPageMessage());
    }

    @Test
    public void test_login_Incorrect(){
        LoginPage login = new LoginPage(driver);
        login.login("malId", "malPassword");
        String mensaje = login.getMessageAlert();
        Assertions.assertEquals("User is not valid", mensaje);
    }
}
