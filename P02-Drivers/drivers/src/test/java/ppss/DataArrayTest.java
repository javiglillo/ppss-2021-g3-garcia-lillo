package ppss;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class DataArrayTest {

    @Test
    public void C1_DataArray() {
        int[] coleccion = {1,3,5,7};
        DataArray da = new DataArray(coleccion);
        int [] arrayEsperado = {1,3,7};
        int numElementosEsperado = 3;

        assertDoesNotThrow(() -> da.delete(5), "Excepcion lanzada");
        assertAll(
                () -> assertArrayEquals(arrayEsperado, da.getColeccion()),
                () -> assertEquals(numElementosEsperado, da.size())
        );
    }

    @Test
    public void C2_DataArray() {
        int[] coleccion = {1,3,3,5,7};
        DataArray da = new DataArray(coleccion);
        int [] arrayEsperado = {1,3,5,7};
        int numElementosEsperado = 4;

        assertDoesNotThrow(() -> da.delete(3), "Excepcion lanzada");
        assertAll(
                () -> assertArrayEquals(arrayEsperado, da.getColeccion()),
                () -> assertEquals(numElementosEsperado, da.size())
        );
    }

    @Test
    public void C3_DataArray() {
        int[] coleccion = {1,2,3,4,5,6,7,8,9,10};
        DataArray da = new DataArray(coleccion);
        int [] arrayEsperado = {1,2,3,5,6,7,8,9,10};
        int numElementosEsperado = 9;

        assertDoesNotThrow(() -> da.delete(4), "Excepcion lanzada");
        assertAll(
                () -> assertArrayEquals(arrayEsperado, da.getColeccion()),
                () -> assertEquals(numElementosEsperado, da.size())
        );
    }

    @Test
    public void C4_DataArray() {
        DataArray da = new DataArray();
        DataException resultadoEsperado = assertThrows(DataException.class, () -> da.delete(8));

        assertEquals(resultadoEsperado.getMessage(), "No hay elementos en la colección");
    }

    @Test
    public void C5_DataArray() {
        int [] coleccion = {1,3,5,7};
        DataArray da = new DataArray(coleccion);
        DataException resultadoEsperado = assertThrows(DataException.class, () -> da.delete(-5));

        assertEquals(resultadoEsperado.getMessage(), "El valor a borrar debe ser > cero");
    }

    @Test
    public void C6_DataArray() {
        DataArray da = new DataArray();
        DataException resultadoEsperado = assertThrows(DataException.class, () -> da.delete(0));

        assertEquals(resultadoEsperado.getMessage(), "Colección vacía. "+
                "Y el valor a borrar debe ser > 0");
    }

    @Test
    public void C7_DataArray() {
        int [] coleccion = {1,3,5,7};
        DataArray da = new DataArray(coleccion);
        DataException resultadoEsperado = assertThrows(DataException.class, () -> da.delete(8));

        assertEquals(resultadoEsperado.getMessage(), "Elemento no encontrado");
    }
}