package ppss;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.*;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

@Tag("parametrizados")
public class MatriculaParamTest {
    Matricula mat= new Matricula();

    @ParameterizedTest
    @MethodSource("casosDePrueba")
    public void testParametrizado(float expected, int edad, boolean familiaNumerosa, boolean repetidor) {
        assertEquals(expected, mat.calculaTasaMatricula(edad,familiaNumerosa,repetidor),0.002f);
    }

    private static Stream<Arguments> casosDePrueba(){
        return Stream.of(
                Arguments.of(2000.00f, 20, false, true),
                Arguments.of(250.00f, 18, true, false),
                Arguments.of(500.00f, 19, false, false),
                Arguments.of(250.00f, 70, false, true),
                Arguments.of(400.00f, 60, true, true)
        );
    }
}
