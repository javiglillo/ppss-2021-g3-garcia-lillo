package ppss;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.*;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

@Tag("parametrizados")
public class LlanosParamTest {
    Llanos l = new Llanos();

    @ParameterizedTest
    @MethodSource("casosDePrueba")
    public void testParametrizado(Tramo expected, ArrayList<Integer> lecturas) {
        assertEquals(expected, l.buscarTramoLlanoMasLargo(lecturas));
    }

    private static Stream<Arguments> casosDePrueba(){
        ArrayList<Integer> c1 = new ArrayList<>();
        c1.add(3);
        c1.add(3);
        c1.add(3);
        ArrayList<Integer> c2 = new ArrayList<>();
        c2.add(2);
        ArrayList<Integer> c3 = new ArrayList<>();
        c3.add(-1);
        ArrayList<Integer> c4 = new ArrayList<>();
        c4.add(-1);
        c4.add(-1);
        c4.add(-1);
        c4.add(-1);
        ArrayList<Integer> c5 = new ArrayList<>();
        c5.add(120);
        c5.add(140);
        c5.add(-10);
        c5.add(-10);
        c5.add(-10);

        return Stream.of(
            Arguments.of(new Tramo(0,2), c1),
            Arguments.of(new Tramo(0, 0), c2),
            Arguments.of(new Tramo(0, 1), c3),
            Arguments.of(new Tramo(0, 4), c4),
            Arguments.of(new Tramo(2, 2), c5)
        );
    }
}
