package ppss;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

@Tag("noparametrizados")
class LlanosTest {
    ArrayList<Integer> lecturas = new ArrayList<>();
    float resultadoReal, resultadoEsperado;
    Llanos llanos = new Llanos();

    @Test
    @Tag("tablaA")
    void C1A_buscarTramoLllano() {
        lecturas.add(3);
        lecturas.add(3);
        lecturas.add(3);
        Tramo resultadoReal = llanos.buscarTramoLlanoMasLargo(lecturas);
        Tramo resultadoEsperado = new Tramo(0,2);

        assertEquals(resultadoEsperado, resultadoReal);
    }

    @Test
    @Tag("tablaA")
    void C2A_buscarTramoLllano() {
        lecturas.add(2);
        Tramo resultadoReal = llanos.buscarTramoLlanoMasLargo(lecturas);
        Tramo resultadoEsperado = new Tramo(0,0);

        assertEquals(resultadoEsperado, resultadoReal);
    }

    @Test
    @Tag("tablaB")
    void C1B_buscarTramoLllano() {
        lecturas.add(-1);
        Tramo resultadoReal = llanos.buscarTramoLlanoMasLargo(lecturas);
        Tramo resultadoEsperado = new Tramo(0,1);

        assertEquals(resultadoEsperado, resultadoReal);
    }

    @Test
    @Tag("tablaB")
    void C2B_buscarTramoLllano() {
        lecturas.add(-1);
        lecturas.add(-1);
        lecturas.add(-1);
        lecturas.add(-1);

        Tramo resultadoReal = llanos.buscarTramoLlanoMasLargo(lecturas);
        Tramo resultadoEsperado = new Tramo(0,4);

        assertEquals(resultadoEsperado, resultadoReal);
    }

    @Test
    @Tag("tablaB")
    void C3B_buscarTramoLllano() {
        lecturas.add(120);
        lecturas.add(140);
        lecturas.add(-10);
        lecturas.add(-10);
        lecturas.add(-10);

        Tramo resultadoReal = llanos.buscarTramoLlanoMasLargo(lecturas);
        Tramo resultadoEsperado = new Tramo(2,2);

        assertEquals(resultadoEsperado, resultadoReal);
    }
}