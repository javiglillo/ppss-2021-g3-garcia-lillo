package ppss;
import org.easymock.EasyMock;
import org.easymock.IMocksControl;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class GestorLlamadasTest {

    @Test
    public void casoDePrueba1(){
        IMocksControl ctrl = EasyMock.createStrictControl();
        GestorLlamadas dep1 = EasyMock.partialMockBuilder(GestorLlamadas.class).
                addMockedMethod("getCalendario").createMock(ctrl);
        Calendario dep2 = ctrl.createMock(Calendario.class);

        EasyMock.expect(dep1.getCalendario()).andReturn(dep2);
        EasyMock.expect(dep2.getHoraActual()).andReturn(10);
        ctrl.replay();

        assertEquals(457.6, dep1.calculaConsumo(22), 0.0f);
        ctrl.verify();
    }

    @Test
    public void casoDePrueba2(){
        IMocksControl ctrl = EasyMock.createStrictControl();
        GestorLlamadas dep1 = EasyMock.partialMockBuilder(GestorLlamadas.class).
                addMockedMethod("getCalendario").createMock(ctrl);
        Calendario dep2 = ctrl.createMock(Calendario.class);

        EasyMock.expect(dep1.getCalendario()).andReturn(dep2);
        EasyMock.expect(dep2.getHoraActual()).andReturn(21);
        ctrl.replay();

        assertEquals(136.5, dep1.calculaConsumo(13), 0.0f);
        ctrl.verify();
    }
}
