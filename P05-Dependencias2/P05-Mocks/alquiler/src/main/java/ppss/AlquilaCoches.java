package ppss;

import java.time.*;

public class AlquilaCoches {
    protected Calendario calendario = new Calendario();

    public Ticket calculaPrecio(TipoCoche tipo, LocalDate inicio, int ndias, Servicio servicio) throws MensajeException {
        Ticket ticket = new Ticket();
        float precioDia, precioTotal = 0.0f;
        float porcentaje = 0.25f;

        String observaciones = "";
        precioDia = servicio.consultaPrecio(tipo); //dep1 -> clase Servicio
        for(int i = 0;i < ndias; i++){
            LocalDate otroDia = inicio.plusDays((long) i);
            try{
                if(calendario.es_festivo(otroDia)){ //dep2 -> clase Calendario
                    precioTotal += (1 + porcentaje) * precioDia;
                }
                else{
                    precioTotal += (1 - porcentaje) * precioDia;
                }
            } catch(CalendarioException ex){
                observaciones += "Error en dia: " + otroDia + "; ";
            }
        }
        if(observaciones.length()  > 0){
            throw new MensajeException(observaciones);
        }

        ticket.setPrecio_final(precioTotal);
        return ticket;
    }
}
