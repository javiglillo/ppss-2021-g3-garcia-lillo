package ppss;

import org.easymock.*;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.Month;

import static org.junit.jupiter.api.Assertions.*;

public class AlquilaCochesTest {
    AlquilaCoches a = new AlquilaCoches();
    Ticket resultadoEsperado = new Ticket();
    Ticket resultadoReal;

    @Test
    public void casoDePrueba1(){
        IMocksControl ctrl = EasyMock.createStrictControl();
        Servicio dep1 = ctrl.createMock(Servicio.class);
        Calendario dep2 = ctrl.createMock(Calendario.class);
        a.calendario = dep2;
        LocalDate fechaInicio = LocalDate.of(2021, Month.MAY, 18);

        EasyMock.expect(dep1.consultaPrecio(TipoCoche.TURISMO)).andReturn(10);
        assertDoesNotThrow(() -> EasyMock.expect(dep2.es_festivo(EasyMock.anyObject())).andReturn(false).times(10),
                "Excepcion lanzada");
        ctrl.replay();

        resultadoEsperado.setPrecio_final(75.0f);
        assertDoesNotThrow(() -> resultadoReal = a.calculaPrecio(TipoCoche.TURISMO, fechaInicio, 10, dep1), "Excepcion lanzada");
        assertAll(
                () -> assertEquals(resultadoEsperado, resultadoReal)
        );
        ctrl.verify();
    }

    @Test
    public void casoDePrueba2(){
        IMocksControl ctrl = EasyMock.createStrictControl();
        Servicio dep1 = ctrl.createMock(Servicio.class);
        Calendario dep2 = ctrl.createMock(Calendario.class);
        a.calendario = dep2;
        LocalDate fechaInicio = LocalDate.of(2021, Month.JUNE, 19);

        EasyMock.expect(dep1.consultaPrecio(TipoCoche.CARAVANA)).andReturn(10);
        assertDoesNotThrow(() -> EasyMock.expect(dep2.es_festivo(EasyMock.anyObject())).andReturn(false).times(1).
                andReturn(true).times(1).
                andReturn(false).times(3).
                andReturn(true).times(1).
                andReturn(false).times(1), "Excepcion lanzada");
        ctrl.replay();

        resultadoEsperado.setPrecio_final(62.5f);
        assertDoesNotThrow(() -> resultadoReal = a.calculaPrecio(TipoCoche.CARAVANA, fechaInicio, 7, dep1), "Excepcion lanzada");
        assertAll(
                () -> assertEquals(resultadoEsperado, resultadoReal)
        );
        ctrl.verify();
    }

    @Test
    public void casoDePrueba3(){
        IMocksControl ctrl = EasyMock.createStrictControl();
        Servicio dep1 = ctrl.createMock(Servicio.class);
        Calendario dep2 = ctrl.createMock(Calendario.class);
        a.calendario = dep2;
        LocalDate fechaInicio = LocalDate.of(2021, Month.APRIL, 17);

        EasyMock.expect(dep1.consultaPrecio(TipoCoche.TURISMO)).andReturn(10);
        assertDoesNotThrow(() -> EasyMock.expect(dep2.es_festivo(EasyMock.anyObject())).andReturn(false).times(1).
                andThrow(new CalendarioException("Error en dia: " + "2021-04-18" + "; ")).times(1).
                andReturn(false).times(2).
                andThrow(new CalendarioException("Error en dia: " + "2021-04-21" + "; ")).times(1).
                andThrow(new CalendarioException("Error en dia: " + "2021-04-22" + "; ")).times(1).
                andReturn(false).times(2), "Excepcion lanzada");
        ctrl.replay();

        MensajeException resultadoReal = assertThrows(MensajeException.class, () -> a.calculaPrecio(TipoCoche.TURISMO,
                fechaInicio, 8, dep1));

        assertEquals("Error en dia: 2021-04-18; " + "Error en dia: 2021-04-21; " + "Error en dia: 2021-04-22; ",
                resultadoReal.getMessage());
        ctrl.verify();
    }

    @Test
    public void casoDePrueba3A(){
        IMocksControl ctrl = EasyMock.createStrictControl();
        Servicio dep1 = ctrl.createMock(Servicio.class);
        Calendario dep2 = ctrl.createMock(Calendario.class);
        a.calendario = dep2;
        LocalDate fechaInicio = LocalDate.of(2021, Month.APRIL, 17);


        EasyMock.expect(dep1.consultaPrecio(TipoCoche.TURISMO)).andReturn(10);
        assertDoesNotThrow(() -> EasyMock.expect(dep2.es_festivo(LocalDate.of(2021, Month.APRIL, 17))).andReturn(false)
                , "Excepcion lanzada");
        assertDoesNotThrow(() -> EasyMock.expect(dep2.es_festivo(LocalDate.of(2021, Month.APRIL, 18))).
                        andThrow(new CalendarioException("Error en dia: " + "2021-04-18" + "; ")), "Excepcion lanzada");
        assertDoesNotThrow(() -> EasyMock.expect(dep2.es_festivo(LocalDate.of(2021, Month.APRIL, 19))).andReturn(false)
                , "Excepcion lanzada");
        assertDoesNotThrow(() -> EasyMock.expect(dep2.es_festivo(LocalDate.of(2021, Month.APRIL, 20))).andReturn(false)
                , "Excepcion lanzada");
        assertDoesNotThrow(() -> EasyMock.expect(dep2.es_festivo(LocalDate.of(2021, Month.APRIL, 21))).
                        andThrow(new CalendarioException("Error en dia: " + "2021-04-21" + "; ")), "Excepcion lanzada");
        assertDoesNotThrow(() -> EasyMock.expect(dep2.es_festivo(LocalDate.of(2021, Month.APRIL, 22))).
                        andThrow(new CalendarioException("Error en dia: " + "2021-04-22" + "; ")), "Excepcion lanzada");
        assertDoesNotThrow(() -> EasyMock.expect(dep2.es_festivo(LocalDate.of(2021, Month.APRIL, 23))).andReturn(false)
                , "Excepcion lanzada");
        assertDoesNotThrow(() -> EasyMock.expect(dep2.es_festivo(LocalDate.of(2021, Month.APRIL, 24))).andReturn(false)
                , "Excepcion lanzada");
        ctrl.replay();

        MensajeException resultadoReal = assertThrows(MensajeException.class, () -> a.calculaPrecio(TipoCoche.TURISMO,
                fechaInicio, 8, dep1));

        assertEquals("Error en dia: 2021-04-18; " + "Error en dia: 2021-04-21; " + "Error en dia: 2021-04-22; ",
                resultadoReal.getMessage());
        ctrl.verify();
    }
}