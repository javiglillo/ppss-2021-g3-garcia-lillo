package ppss;

import java.util.ArrayList;
import ppss.excepciones.*;

public class Reserva {
    public boolean compruebaPermisos(String login, String password, Usuario tipoUsu){
        throw new UnsupportedOperationException("Not yet implemented");
    }

    public FactoriaBOs getFactoria(){
        return new FactoriaBOs();
    }

    public void realizaReserva(String login, String password, String socio, String [] isbns) throws Exception{
        ArrayList<String> errores = new ArrayList<>();

        if(!compruebaPermisos(login, password, Usuario.BIBLIOTECARIO)){ //dependencia1 -> clase Reserva
            errores.add("ERROR de permisos");
        }
        else{
            FactoriaBOs fd = getFactoria();
            IOperacionBO io = fd.getOperacionBO(); //dependencia 2 -> clase FactoriaBOs
            try{
                for(String isbn : isbns){
                    try{
                        io.operacionReserva(socio, isbn); //dependencia 3 -> clase Operacion
                    } catch(IsbnInvalidoException iie){
                        errores.add("ISBN invalido" + ":" + isbn);
                    }
                }
            } catch(SocioInvalidoException sie){
                errores.add("SOCIO invalido");
            } catch(JDBCException je){
                errores.add("CONEXION invalida");
            }
        }
        if(errores.size() > 0){
            String mensajeError = "";
            for(String error : errores){
                mensajeError += error + "; ";
            }
            throw new ReservaException(mensajeError);
        }
    }
}
