package ppss;

import ppss.excepciones.*;

public interface IOperacionBO {
    public abstract void operacionReserva(String socio, String isbn) throws IsbnInvalidoException, JDBCException, SocioInvalidoException;
}

