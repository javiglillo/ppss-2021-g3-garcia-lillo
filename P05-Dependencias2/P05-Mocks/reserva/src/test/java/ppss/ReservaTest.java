package ppss;

import org.easymock.EasyMock;
import org.easymock.IMocksControl;
import org.junit.jupiter.api.Test;
import ppss.excepciones.*;

import static org.junit.jupiter.api.Assertions.*;

public class ReservaTest {

    @Test
    public void casoDePrueba1C(){
        Reserva dep1 = EasyMock.partialMockBuilder(Reserva.class).
                addMockedMethod("compruebaPermisos").createMock();
        String login = "xxxx", password= "xxxx", socio = "Pepe";
        String[] isbns = {"22222"};


        EasyMock.expect(dep1.compruebaPermisos(login, password, Usuario.BIBLIOTECARIO)).andReturn(false);
        EasyMock.replay(dep1);
        ReservaException resultadoEsperado = assertThrows( ReservaException.class,
                () -> dep1.realizaReserva(login, password, socio, isbns));

        assertEquals(resultadoEsperado.getMessage(), "ERROR de permisos; ");
        EasyMock.verify(dep1);
    }

    @Test
    public void casoDePrueba2C(){
        IMocksControl ctrl = EasyMock.createStrictControl();
        Reserva dep1 = EasyMock.partialMockBuilder(Reserva.class).
                addMockedMethods("compruebaPermisos", "getFactoria").createMock(ctrl);
        FactoriaBOs dep2 = ctrl.createMock(FactoriaBOs.class);
        Operacion dep3 = ctrl.createMock(Operacion.class);
        String[] isbns = {"22222", "33333"};
        String socio = "Pepe", login = "ppss", password = "ppss";

        EasyMock.expect(dep1.compruebaPermisos(login, password, Usuario.BIBLIOTECARIO)).andReturn(true);
        EasyMock.expect(dep1.getFactoria()).andReturn(dep2);
        EasyMock.expect(dep2.getOperacionBO()).andReturn(dep3);
        for(int i = 0;i < isbns.length;i++){
            int finalI = i;
            assertDoesNotThrow(() -> dep3.operacionReserva(socio, isbns[finalI]), "Excepcion lanzada");
            EasyMock.expectLastCall();
        }
        ctrl.replay();

        assertDoesNotThrow(() -> dep1.realizaReserva(login, password, socio, isbns),
                "Excepcion lanzada");
        ctrl.verify();
    }

    @Test
    public void casoDePrueba3C(){
        IMocksControl ctrl = EasyMock.createStrictControl();
        Reserva dep1 = EasyMock.partialMockBuilder(Reserva.class).
                addMockedMethods("compruebaPermisos", "getFactoria").createMock(ctrl);
        FactoriaBOs dep2 = ctrl.createMock(FactoriaBOs.class);
        Operacion dep3 = ctrl.createMock(Operacion.class);
        String[] isbns = {"11111"};
        String socio = "Pepe", login = "ppss", password = "ppss";

        EasyMock.expect(dep1.compruebaPermisos(login, password, Usuario.BIBLIOTECARIO)).andReturn(true);
        EasyMock.expect(dep1.getFactoria()).andReturn(dep2);
        EasyMock.expect(dep2.getOperacionBO()).andReturn(dep3);
        for(int i = 0;i < isbns.length;i++){
            int finalI = i;
            assertDoesNotThrow(() -> dep3.operacionReserva(socio, isbns[finalI]), "Excepcion lanzada");
            EasyMock.expectLastCall().andThrow(new IsbnInvalidoException());
        }
        ctrl.replay();

        ReservaException resultadoReal = assertThrows(ReservaException.class,
                () -> dep1.realizaReserva(login, password, socio, isbns));
        assertEquals("ISBN invalido:11111; ", resultadoReal.getMessage());
        ctrl.verify();
    }

    @Test
    public void casoDePrueba4C(){
        IMocksControl ctrl = EasyMock.createStrictControl();
        Reserva dep1 = EasyMock.partialMockBuilder(Reserva.class).
                addMockedMethods("compruebaPermisos", "getFactoria").createMock(ctrl);
        FactoriaBOs dep2 = ctrl.createMock(FactoriaBOs.class);
        Operacion dep3 = ctrl.createMock(Operacion.class);
        String[] isbns = {"22222"};
        String socio = "Luis", login = "ppss", password = "ppss";

        EasyMock.expect(dep1.compruebaPermisos(login, password, Usuario.BIBLIOTECARIO)).andReturn(true);
        EasyMock.expect(dep1.getFactoria()).andReturn(dep2);
        EasyMock.expect(dep2.getOperacionBO()).andReturn(dep3);
        for(int i = 0;i < isbns.length;i++){
            int finalI = i;
            assertDoesNotThrow(() -> dep3.operacionReserva(socio, isbns[finalI]), "Excepcion lanzada");
            EasyMock.expectLastCall().andThrow(new SocioInvalidoException());
        }
        ctrl.replay();

        ReservaException resultadoReal = assertThrows(ReservaException.class,
                () -> dep1.realizaReserva(login, password, socio, isbns));
        assertEquals("SOCIO invalido; ", resultadoReal.getMessage());
        ctrl.verify();
    }

    @Test
    public void casoDePrueba5C(){
        IMocksControl ctrl = EasyMock.createStrictControl();
        Reserva dep1 = EasyMock.partialMockBuilder(Reserva.class).
                addMockedMethods("compruebaPermisos", "getFactoria").createMock(ctrl);
        FactoriaBOs dep2 = ctrl.createMock(FactoriaBOs.class);
        Operacion dep3 = ctrl.createMock(Operacion.class);
        String[] isbns = {"11111", "22222", "33333"};
        String socio = "Pepe", login = "ppss", password = "ppss";

        EasyMock.expect(dep1.compruebaPermisos(login, password, Usuario.BIBLIOTECARIO)).andReturn(true);
        EasyMock.expect(dep1.getFactoria()).andReturn(dep2);
        EasyMock.expect(dep2.getOperacionBO()).andReturn(dep3);
        for(int i = 0;i < isbns.length;i++){
            int finalI = i;
            if(isbns[finalI] == "11111"){
                assertDoesNotThrow(() -> dep3.operacionReserva(socio, isbns[finalI]), "Excepcion lanzada");
                EasyMock.expectLastCall().andThrow(new IsbnInvalidoException());
            }
            else if(isbns[finalI] == "22222"){
                assertDoesNotThrow(() -> dep3.operacionReserva(socio, isbns[finalI]), "Excepcion lanzada");
                EasyMock.expectLastCall();
            }
            else{
                assertDoesNotThrow(() -> dep3.operacionReserva(socio, isbns[finalI]), "Excepcion lanzada");
                EasyMock.expectLastCall().andThrow(new JDBCException());
            }
        }
        ctrl.replay();

        ReservaException resultadoReal = assertThrows(ReservaException.class,
                () -> dep1.realizaReserva(login, password, socio, isbns));
        assertEquals("ISBN invalido:11111; CONEXION invalida; ", resultadoReal.getMessage());
        ctrl.verify();
    }

    @Test
    public void casoDePrueba1E(){
        Reserva reservaStub = EasyMock.niceMock(Reserva.class);
        String login = "xxxx", password= "xxxx", socio = "Pepe";
        String[] isbns = {"22222"};

        EasyMock.expect(reservaStub.compruebaPermisos(login, password, Usuario.BIBLIOTECARIO)).andStubReturn(false);
        assertDoesNotThrow(() -> reservaStub.realizaReserva(login, password, socio, isbns), "Excepcion lanzada");
        EasyMock.expectLastCall().andStubThrow(new ReservaException("ERROR de permisos; "));
        EasyMock.replay(reservaStub);

        ReservaException resultadoEsperado = assertThrows(ReservaException.class,
                () -> reservaStub.realizaReserva(login, password, socio, isbns));

        assertEquals(resultadoEsperado.getMessage(), "ERROR de permisos; ");
    }

    @Test
    public void casoDePrueba2E(){
        Reserva dep1 = EasyMock.niceMock(Reserva.class);
        FactoriaBOs dep2 = EasyMock.niceMock(FactoriaBOs.class);
        Operacion dep3 = EasyMock.niceMock(Operacion.class);
        String[] isbns = {"22222", "33333"};
        String socio = "Pepe", login = "ppss", password = "ppss";

        EasyMock.expect(dep1.compruebaPermisos(login, password, Usuario.BIBLIOTECARIO)).andStubReturn(true);
        EasyMock.expect(dep1.getFactoria()).andStubReturn(dep2);
        EasyMock.expect(dep2.getOperacionBO()).andStubReturn(dep3);
        for(int i = 0;i < isbns.length;i++){
            int finalI = i;
            assertDoesNotThrow(() -> dep3.operacionReserva(socio, isbns[finalI]), "Excepcion lanzada");
            EasyMock.expectLastCall();
        }
        EasyMock.replay(dep1, dep2, dep3);

        assertDoesNotThrow(() -> dep1.realizaReserva(login, password, socio, isbns),
                "Excepcion lanzada");
    }

    @Test
    public void casoDePrueba3E(){
        Reserva dep1 = EasyMock.niceMock(Reserva.class);
        FactoriaBOs dep2 = EasyMock.niceMock(FactoriaBOs.class);
        Operacion dep3 = EasyMock.niceMock(Operacion.class);
        String[] isbns = {"11111"};
        String socio = "Pepe", login = "ppss", password = "ppss";

        EasyMock.expect(dep1.compruebaPermisos(login, password, Usuario.BIBLIOTECARIO)).andStubReturn(true);
        EasyMock.expect(dep1.getFactoria()).andStubReturn(dep2);
        EasyMock.expect(dep2.getOperacionBO()).andStubReturn(dep3);
        for(int i = 0;i < isbns.length;i++){
            int finalI = i;
            assertDoesNotThrow(() -> dep3.operacionReserva(socio, isbns[finalI]), "Excepcion lanzada");
            EasyMock.expectLastCall().andStubThrow(new IsbnInvalidoException());
        }
        assertDoesNotThrow(() -> dep1.realizaReserva(login, password, socio, isbns), "Excepcion lanzada");
        EasyMock.expectLastCall().andStubThrow(new ReservaException("ISBN invalido:11111; "));
        EasyMock.replay(dep1, dep2, dep3);

        ReservaException resultadoReal = assertThrows(ReservaException.class,
                () -> dep1.realizaReserva(login, password, socio, isbns));
        assertEquals("ISBN invalido:11111; ", resultadoReal.getMessage());
    }

    @Test
    public void casoDePrueba4E(){
        Reserva dep1 = EasyMock.niceMock(Reserva.class);
        FactoriaBOs dep2 = EasyMock.niceMock(FactoriaBOs.class);
        Operacion dep3 = EasyMock.niceMock(Operacion.class);
        String[] isbns = {"22222"};
        String socio = "Luis", login = "ppss", password = "ppss";

        EasyMock.expect(dep1.compruebaPermisos(login, password, Usuario.BIBLIOTECARIO)).andReturn(true);
        EasyMock.expect(dep1.getFactoria()).andReturn(dep2);
        EasyMock.expect(dep2.getOperacionBO()).andReturn(dep3);
        for(int i = 0;i < isbns.length;i++){
            int finalI = i;
            assertDoesNotThrow(() -> dep3.operacionReserva(socio, isbns[finalI]), "Excepcion lanzada");
            EasyMock.expectLastCall().andThrow(new SocioInvalidoException());
        }
        assertDoesNotThrow(() -> dep1.realizaReserva(login, password, socio, isbns), "Excepcion lanzada");
        EasyMock.expectLastCall().andStubThrow(new ReservaException("SOCIO invalido; "));
        EasyMock.replay(dep1, dep2, dep3);

        ReservaException resultadoReal = assertThrows(ReservaException.class,
                () -> dep1.realizaReserva(login, password, socio, isbns));
        assertEquals("SOCIO invalido; ", resultadoReal.getMessage());
    }

    @Test
    public void casoDePrueba5E(){
        Reserva dep1 = EasyMock.niceMock(Reserva.class);
        FactoriaBOs dep2 = EasyMock.niceMock(FactoriaBOs.class);
        Operacion dep3 = EasyMock.niceMock(Operacion.class);
        String[] isbns = {"11111", "22222", "33333"};
        String socio = "Pepe", login = "ppss", password = "ppss";

        EasyMock.expect(dep1.compruebaPermisos(login, password, Usuario.BIBLIOTECARIO)).andReturn(true);
        EasyMock.expect(dep1.getFactoria()).andReturn(dep2);
        EasyMock.expect(dep2.getOperacionBO()).andReturn(dep3);
        for(int i = 0;i < isbns.length;i++){
            int finalI = i;
            if(isbns[finalI] == "11111"){
                assertDoesNotThrow(() -> dep3.operacionReserva(socio, isbns[finalI]), "Excepcion lanzada");
                EasyMock.expectLastCall().andThrow(new IsbnInvalidoException());
            }
            else if(isbns[finalI] == "22222"){
                assertDoesNotThrow(() -> dep3.operacionReserva(socio, isbns[finalI]), "Excepcion lanzada");
                EasyMock.expectLastCall();
            }
            else{
                assertDoesNotThrow(() -> dep3.operacionReserva(socio, isbns[finalI]), "Excepcion lanzada");
                EasyMock.expectLastCall().andThrow(new JDBCException());
            }
        }
        assertDoesNotThrow(() -> dep1.realizaReserva(login, password, socio, isbns), "Excepcion lanzada");
        EasyMock.expectLastCall().andStubThrow(new ReservaException("ISBN invalido:11111; CONEXION invalida; "));
        EasyMock.replay(dep1, dep2, dep3);

        ReservaException resultadoReal = assertThrows(ReservaException.class,
                () -> dep1.realizaReserva(login, password, socio, isbns));
        assertEquals("ISBN invalido:11111; CONEXION invalida; ", resultadoReal.getMessage());
    }
}