package ppss;

import org.junit.jupiter.api.Test;
import org.easymock.*;

import static org.junit.jupiter.api.Assertions.*;

public class PremioTest {

    @Test
    public void casoDePrueba1(){
        IMocksControl ctrl = EasyMock.createStrictControl();
        Premio dep1 = EasyMock.partialMockBuilder(Premio.class).
                addMockedMethod("generaNumero").createMock(ctrl);
        ClienteWebService dep2 = ctrl.createMock(ClienteWebService.class);
        dep1.cliente = dep2;

        EasyMock.expect(dep1.generaNumero()).andReturn(0.07f);
        assertDoesNotThrow(() -> EasyMock.expect(dep2.obtenerPremio()).andReturn("entrada final Champions"), "Excepcion lanzada");
        ctrl.replay();

        assertEquals("Premiado con entrada final Champions", dep1.compruebaPremio());
        ctrl.verify();
    }

    @Test
    public void casoDePrueba2(){
        IMocksControl ctrl = EasyMock.createStrictControl();
        Premio dep1 = EasyMock.partialMockBuilder(Premio.class).
                addMockedMethod("generaNumero").createMock(ctrl);
        ClienteWebService dep2 = ctrl.createMock(ClienteWebService.class);
        dep1.cliente = dep2;

        EasyMock.expect(dep1.generaNumero()).andReturn(0.07f);
        assertDoesNotThrow(() -> EasyMock.expect(dep2.obtenerPremio()).andThrow(new ClienteWebServiceException()),
            "Excepcion lanzada");
        ctrl.replay();

        assertEquals("No se ha podido obtener el premio", dep1.compruebaPremio());
        ctrl.verify();
    }

    @Test
    public void casoDePrueba3(){
        Premio dep1 = EasyMock.partialMockBuilder(Premio.class).
                addMockedMethod("generaNumero").createMock();

        EasyMock.expect(dep1.generaNumero()).andReturn(0.3f);
        EasyMock.replay(dep1);

        assertEquals("Sin premio", dep1.compruebaPremio());
        EasyMock.verify(dep1);
    }
}