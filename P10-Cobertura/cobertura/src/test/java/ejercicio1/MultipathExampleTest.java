package ejercicio1;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class MultipathExampleTest {
    MultipathExample m = new MultipathExample();

    @ParameterizedTest
    @MethodSource("casosDePrueba1")
    public void testParametrizado1(int expected, int a, int b, int c) {
        assertEquals(expected, m.multipath1(a, b, c));
    }

    private static Stream<Arguments> casosDePrueba1(){
        return Stream.of(
                Arguments.of(13, 6, 6, 1),
                Arguments.of(1, 4, 4, 1)
        );
    }

    @ParameterizedTest
    @MethodSource("casosDePrueba2")
    public void testParametrizado2(int expected, int a, int b, int c) {
        assertEquals(expected, m.multipath2(a, b, c));
    }

    private static Stream<Arguments> casosDePrueba2(){
        return Stream.of(
                Arguments.of(16, 6, 4, 6),
                Arguments.of(4, 4, 4, 4),
                Arguments.of(12, 6, 6, 6)
        );
    }

    @ParameterizedTest
    @MethodSource("casosDePrueba3")
    public void testParametrizado3(int expected, int a, int b, int c) {
        assertEquals(expected, m.multipath3(a, b, c));
    }

    private static Stream<Arguments> casosDePrueba3(){
        return Stream.of(
                Arguments.of(16, 6, 4, 6),
                Arguments.of(4, 4, 4, 4),
                Arguments.of(12, 6, 6, 6)
        );
    }

}