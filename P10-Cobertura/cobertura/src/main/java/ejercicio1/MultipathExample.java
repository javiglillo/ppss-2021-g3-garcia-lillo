package ejercicio1;

public class MultipathExample {

    public int multipath1(int a, int b, int c){
        if(a > 5){
            c += a;
        }
        if(b > 5){
            c += b;
        }
        return c;
    }

    public int multipath2(int a, int b, int c){
        if((a > 5) && (b < 5)){
            b += a;
        }
        if(c > 5){
            c += b;
        }
        return c;
    }

    public int multipath3(int a, int b, int c){
        if((a > 5) & (b < 5)){
            b += a;
        }
        if(c > 5){
            c += b;
        }
        return c;
    }
}
