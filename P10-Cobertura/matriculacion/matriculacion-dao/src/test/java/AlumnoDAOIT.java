import org.dbunit.Assertion;
import org.dbunit.IDatabaseTester;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.util.fileloader.FlatXmlDataFileLoader;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

import ppss.matriculacion.dao.DAOException;
import ppss.matriculacion.dao.FactoriaDAO;
import ppss.matriculacion.dao.IAlumnoDAO;
import ppss.matriculacion.to.AlumnoTO;

import java.time.LocalDate;
import java.time.Month;

@Tag("Integracion-fase1")
public class AlumnoDAOIT {
    private IAlumnoDAO alumnoDAO; //SUT
    private IDatabaseTester databaseTester;
    private IDatabaseConnection connection;

    @BeforeEach
    public void setUp() throws Exception {

        String cadena_conexionDB = "jdbc:mysql://localhost:3306/matriculacion?useSSL=false";
        databaseTester = new MiJdbcDatabaseTester("com.mysql.cj.jdbc.Driver",
                cadena_conexionDB, "root", "ppss");
        //obtenemos la conexión con la BD
        connection = databaseTester.getConnection();

        alumnoDAO = new FactoriaDAO().getAlumnoDAO();
    }

    @Test
    public void testA1() throws Exception{
        AlumnoTO ainsertado = new AlumnoTO();
        ainsertado.setNif("33333333C");
        ainsertado.setNombre("Elena Aguirre Juarez");
        ainsertado.setFechaNacimiento(LocalDate.of(1985, Month.FEBRUARY, 22));

        //Inicializamos el dataSet con los datos iniciales de la tabla alumno
        IDataSet dataSet = new FlatXmlDataFileLoader().load("/initA1.xml");
        //Inyectamos el dataset en el objeto databaseTester
        databaseTester.setDataSet(dataSet);
        //inicializamos la base de datos con los contenidos del dataset
        databaseTester.onSetup();
        //invocamos a nuestro SUT
        Assertions.assertDoesNotThrow(()->alumnoDAO.addAlumno(ainsertado));

        //recuperamos los datos de la BD después de invocar al SUT
        IDataSet databaseDataSet = connection.createDataSet();
        //Recuperamos los datos de la tabla alumno
        ITable actualTable = databaseDataSet.getTable("alumnos");

        //creamos el dataset con el resultado esperado
        IDataSet expectedDataSet = new FlatXmlDataFileLoader().load("/tabla3.xml");
        ITable expectedTable = expectedDataSet.getTable("alumnos");

        Assertion.assertEquals(expectedTable, actualTable);
    }

    @Test
    public void testA2() throws Exception{
        AlumnoTO ainsertado = new AlumnoTO();
        ainsertado.setNif("11111111A");
        ainsertado.setNombre("Alfonso Ramirez Ruiz");
        ainsertado.setFechaNacimiento(LocalDate.of(1982, Month.FEBRUARY, 22));

        //Inicializamos el dataSet con los datos iniciales de la tabla alumno
        IDataSet dataSet = new FlatXmlDataFileLoader().load("/initA1.xml");
        //Inyectamos el dataset en el objeto databaseTester
        databaseTester.setDataSet(dataSet);
        //inicializamos la base de datos con los contenidos del dataset
        databaseTester.onSetup();
        //invocamos a nuestro SUT
        DAOException resultadoReal = Assertions.assertThrows(DAOException.class, ()->alumnoDAO.addAlumno(ainsertado));
        assertEquals("Error al conectar con BD", resultadoReal.getMessage());
    }

    @Test
    public void testA3() throws Exception{
        AlumnoTO ainsertado = new AlumnoTO();
        ainsertado.setNif("44444444D");
        ainsertado.setNombre(null);
        ainsertado.setFechaNacimiento(LocalDate.of(1982, Month.FEBRUARY, 22));

        //Inicializamos el dataSet con los datos iniciales de la tabla alumno
        IDataSet dataSet = new FlatXmlDataFileLoader().load("/initA1.xml");
        //Inyectamos el dataset en el objeto databaseTester
        databaseTester.setDataSet(dataSet);
        //inicializamos la base de datos con los contenidos del dataset
        databaseTester.onSetup();
        //invocamos a nuestro SUT
        DAOException resultadoReal = Assertions.assertThrows(DAOException.class, ()->alumnoDAO.addAlumno(ainsertado));
        assertEquals("Error al conectar con BD", resultadoReal.getMessage());
    }

    @Test
    public void testA4() throws Exception{

        //Inicializamos el dataSet con los datos iniciales de la tabla alumno
        IDataSet dataSet = new FlatXmlDataFileLoader().load("/initA1.xml");
        //Inyectamos el dataset en el objeto databaseTester
        databaseTester.setDataSet(dataSet);
        //inicializamos la base de datos con los contenidos del dataset
        databaseTester.onSetup();
        //invocamos a nuestro SUT
        DAOException resultadoReal = Assertions.assertThrows(DAOException.class, ()->alumnoDAO.addAlumno(null));

        assertEquals("Alumno nulo", resultadoReal.getMessage());
    }

    @Test
    public void testA5() throws Exception{
        AlumnoTO ainsertado = new AlumnoTO();
        ainsertado.setNif(null);
        ainsertado.setNombre("Pedro Garcia Lopez");
        ainsertado.setFechaNacimiento(LocalDate.of(1982, Month.FEBRUARY, 22));

        //Inicializamos el dataSet con los datos iniciales de la tabla alumno
        IDataSet dataSet = new FlatXmlDataFileLoader().load("/initA1.xml");
        //Inyectamos el dataset en el objeto databaseTester
        databaseTester.setDataSet(dataSet);
        //inicializamos la base de datos con los contenidos del dataset
        databaseTester.onSetup();
        //invocamos a nuestro SUT
        DAOException resultadoReal = Assertions.assertThrows(DAOException.class, ()->alumnoDAO.addAlumno(ainsertado));
        assertEquals("Error al conectar con BD", resultadoReal.getMessage());
    }

    @Test
    public void testB1() throws Exception{
        //Inicializamos el dataSet con los datos iniciales de la tabla alumno
        IDataSet dataSet = new FlatXmlDataFileLoader().load("/initA1.xml");
        //Inyectamos el dataset en el objeto databaseTester
        databaseTester.setDataSet(dataSet);
        //inicializamos la base de datos con los contenidos del dataset
        databaseTester.onSetup();
        //invocamos a nuestro SUT
        Assertions.assertDoesNotThrow(()->alumnoDAO.delAlumno("11111111A"));

        //recuperamos los datos de la BD después de invocar al SUT
        IDataSet databaseDataSet = connection.createDataSet();
        //Recuperamos los datos de la tabla alumno
        ITable actualTable = databaseDataSet.getTable("alumnos");

        //creamos el dataset con el resultado esperado
        IDataSet expectedDataSet = new FlatXmlDataFileLoader().load("/tabla4.xml");
        ITable expectedTable = expectedDataSet.getTable("alumnos");

        Assertion.assertEquals(expectedTable, actualTable);
    }

    @Test
    public void testB2() throws Exception{
        //Inicializamos el dataSet con los datos iniciales de la tabla alumno
        IDataSet dataSet = new FlatXmlDataFileLoader().load("/initA1.xml");
        //Inyectamos el dataset en el objeto databaseTester
        databaseTester.setDataSet(dataSet);
        //inicializamos la base de datos con los contenidos del dataset
        databaseTester.onSetup();
        //invocamos a nuestro SUT
        DAOException resultadoReal = Assertions.assertThrows(DAOException.class, ()->alumnoDAO.delAlumno("33333333C"));

        assertEquals("No se ha borrado ningun alumno", resultadoReal.getMessage());
    }
}
